# Design and Performance Evaluation of AOMDV Routing Protocol for VANET

Nama    : Anindya Hantari

NRP     : 0511540000116

Kelas   : Jaringan Nirkabel


Modifikasi yang dilakukan adalah dengan menggunakan AOMDV dengan menghitung energi paling minimum pada setiap route reply (RREP). 
- Menggunakan routing protocol AOMDV (Ad-Hoc On-Demand Multipath Distance Vector) merupakan routing protocol perkembangan dari AODV (Ad-Hoc On-Demand Distance Vector) yang memiliki multipath dalam setiap pencarian rute. Multipath tersebut terjadi karena AOMDV akan menerima semua route request (RREQ) yang dikirimkan kemudian rute itu akan disimpan menjadi rute utama dan rute alternatif berdasarkan minimal hop. Rute alternatif akan digunakan apabila rute utama mengalami kerusakan ketika melakukan proses pengiriman paket sehingga tidak perlu melakukan pencarian rute dengan melakukan pengiriman paket secara broadcast dari node sumber ke node-node tetangganya untuk mendapatkan route reply dari node tujuan. 
- Pencarian rute pada AOMDV yang ada pada Gambar 1 dimulai dengan node sumber (S) mengirimkan route request (RREQ) secara broadcast ke node-node yang ada di dekatnya (A dan E) atau node yang menjadi node tetangga dari node sumber. Kemudian dilakukan pengecekan apakah node tersebut merupakan node tujuan (D) atau bukan. Jika merupakan node tujuan, maka node tersebut akan mengirimkan route reply (RREP) namun apabila node tersebut bukan merupakan node tujuan, node tersebut akan melanjutkan mengirimkan route request (RREQ) ke node tetangganya kecuali ke node yang telah mengirimkan route request (RREQ) sebelumnya. 
- Membuat sebuah fungsi untuk melakukan pengecekan energi minimum pada setiap route reply (RREP) yang dilalui. Hal pertama yang  dilakukan adalah melakukan perhitungan energi ketika node tujuan mengirimkan route reply (RREP) ke setiap node yang dilaluinya untuk menuju ke node sumber yang kemudian disimpan pada field energy. Langkah kedua adalah membandingkan masing-masing field energy dan mencari field energy  yang paling kecil (minimum). Setelah didapatkan field energy paling kecil,  mengganti field energy yang dilalui dengan field energy paling minimum tersebut. Hal bertujuan untuk mengatasi apabila node tujuan memiliki lebih dari satu path untuk mengirimkan route reply (RREP) maka akan dipilih path dengan field energy paling minimum.